package me.jakev.lowmade;

import api.mod.StarMod;
import api.mod.config.FileConfiguration;

public class LowMade extends StarMod {
    public static void main(String[] args) { }
    @Override
    public void onGameStart() {
        setModName("LowMade");
        setModVersion("1.0");
        setModDescription("Ultra low resolution texture pack for starmade");
        setModAuthor("JakeV");
        setModSMVersion("0.202.101");
    }
    public static int resolution = 1;
    @Override
    public void onEnable() {
        new LowMadeListener();
        FileConfiguration config = getConfig("config.txt");
        resolution = config.getConfigurableInt("resolution", 1);
        config.saveConfig();
    }
}
